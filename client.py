#!/usr/bin/env python3

"""
Print a sample request, send it to the Muse, and print the response.
"""

import sys

from jsonrpcclient.clients.http_client import HTTPClient
from jsonrpcclient.requests import Request

tests = {
    'run_type_refinement': "import sys"}

if __name__ == '__main__':
    client = HTTPClient('http://localhost:4500')
    request = Request("run_type_refinement", source=tests["run_type_refinement"], probs='{}')
    print(f'--> {request}')
    print(f'<-- {client.send(request).data}')
