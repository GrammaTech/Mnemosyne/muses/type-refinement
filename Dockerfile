FROM ubuntu:20.04

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  git python3-dev python3-venv r-base

# install other Python requirements needed for this Muse
COPY requirements.txt .
RUN pip install -r requirements.txt
