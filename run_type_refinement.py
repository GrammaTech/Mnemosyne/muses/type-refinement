#!/usr/bin/env python3

"""
JSON-RPC entrypoint.
"""

from src.type_infer import assign_types
from src.python_source_rep import PythonSourceRepresentation
import json
import tempfile
import pathlib
from http.server import BaseHTTPRequestHandler
from kitchensink.utility.forkinghttpserver import ForkingHTTPServer
import logging
import os
from socketserver import ForkingMixIn
from jsonrpcserver import dispatch, method


class TypeRefinementHttpServer(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'  # to allow persistent connections

    def do_POST(self):
        # Process request
        request = self.rfile.read(int(self.headers["Content-Length"])).decode()
        response = dispatch(request)
        content = str(response).encode()
        # Return response
        self.send_response(response.http_status)
        self.send_header("Content-Length", len(content))
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        self.wfile.write(content)

@method
def run_type_refinement(*, source='', probs=''):
    with tempfile.NamedTemporaryFile() as temp:
        temp.write(bytes(source, encoding='utf-8'))
        temp.seek(0)
        type_probs = json.loads(probs)
        type_predictions = assign_types(PythonSourceRepresentation(temp.name),type_probs)
        return {"prediction list": json.dumps(type_predictions)}


LOG_FORMAT='[%(process)d] '+logging.BASIC_FORMAT


if __name__ == "__main__":
    # to see the port number
    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
    port = int(os.environ['PORT'] or '4500') if 'PORT' in os.environ else 4500
    logging.info(" * Listening on port %s", port)
    with ForkingHTTPServer(("0.0.0.0", port), TypeRefinementHttpServer) as s:
        s.serve_forever()
