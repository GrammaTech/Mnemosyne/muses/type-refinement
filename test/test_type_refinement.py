from pathlib import Path
from src.python_source_rep import PythonSourceRepresentation
from src.type_infer import assign_types

def test_type_writer():
    source_path = Path.cwd() / 'test' / 'source_test_code.py'
    predictions = {'find_match': {'color': [{'str': 0, 'dict': 1, "Union[tf.keras.Model, 'keras.Model']": 2, 'bytes': 3, 'np.ndarray': 4}, (1, 15)], 'return': [{'str': 0, 'dict': 1, "Union[tf.keras.Model, 'keras.Model']": 2, 'bytes': 3, 'np.ndarray': 4}, (1, 0)]}, 'get_colors': {'return': [{'str': 0, 'int': 1, 'Any': 2, 'Dict[str, Any]': 3, 'Optional[bool]': 4}, (13, 0)]}}
    source_rep = PythonSourceRepresentation(str(source_path))


if __name__ == "__main__":
    test_type_writer()
