The assign_types function defined in [type_infer.py](https://gitlab.com/GrammaTech/Mnemosyne/muses/type-refinement/-/blob/master/src/type_infer.py)

Takes in two values, a Source Representation, defined as a subclass of [source_rep.py](https://gitlab.com/GrammaTech/Mnemosyne/muses/type-refinement/-/blob/master/src/source_rep.py) and a Dictionary representing predictions and their defined locations.

## SourceRep 

The SourceRep interface requires two functions to be implemented:

insert_assignment(self, assignment: Dictionary[str, Dictionar[str, str]]))

The assignment is a Dictionary from function names to a Dictionary of variable names to string names of their assigned types, the function is expected to insert the given assignment into whatever source representation is used for the language the code is evaluating

get_type_errors(self) -> (int, list[int])

This function returns the number of errors the current type assignment creates and their locations

For an example of a SourceRep implementation for python using mypy and the python AST class as a way of interacting with source code, see the [python_source_rep.py](https://gitlab.com/GrammaTech/Mnemosyne/muses/type-refinement/-/blob/master/src/python_source_rep.py) class

## Predictions

The predictions Dictionary, in full, has the type: Dict[str, Dict[str, Tuple[Dict[str, int],int]]]. This is a Dictionary from function name to a Dictionary of variable names, to Tuple containing a Dictionary of string (a predicted type assignment) to int (a predicted assignments rank, lower is more probable) and an integer (the variables type assignment location)

For an example, see the test code [here](https://gitlab.com/GrammaTech/Mnemosyne/muses/type-refinement/-/blob/master/test/test_infer.py)
