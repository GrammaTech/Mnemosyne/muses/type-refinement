from typing import Dict, Tuple, List, Set
import copy
import random
import logging
from src.source_rep import SourceRepresentation


class AssignmentState():
    '''
    A state representing a given assignment
    '''

    def __init__(self, assignment, parent, modified_locs):
       self.parent = parent
       self.assignment = assignment
       self.score = None
       self.modified_locs = modified_locs
       self.hashable = self._hashable_rep()

    def _hashable_rep(self):
       '''
       Creates a hashable representation in order to allow sets
       '''
       hashable = ""
       for f_key, f_dict in sorted(self.assignment.items()):
           hashable = hashable + f_key
           for a_key, a_val in sorted(f_dict.items()):
               hashable = hashable + a_key + a_val

       return hashable

    def __eq__(self, other):
       if isinstance(other, AssignmentState):
           return self.hashable == other.hashable
       return False

    def __hash__(self):
       return hash(self.hashable)

    def to_string(self):
       out_str = str(self.assignment)
       return out_str

def assign_types(source_rep: SourceRepresentation, predictions: Dict[str, Dict[str, Tuple[Dict[str, int],Tuple[int,int]]]], greedy=False) -> Dict[str, Dict[str, Tuple[str,Tuple[int,int]]]]:
    '''
    Assigns types using AST replacement based on a given predictions list and SourceRepresentation

    :source_rep: A source representation class that contains the source code
    :predictions: A dictionary containing mappings from function variables to the ranked list of predictions as well as the assignment locations
    :greedy: True if the algorithm will explore states greedily (greedy = faster, but chance to find a local minimum rather than a true minimum)
    :returns: the assignment with the minimum score
    '''

    #Create an initial state of the top predictions
    init_assign: Dict[str, Dict[str, str]] = get_top_predictions(predictions)
    w: int = sum(len(v) for v in init_assign.values())
    init_state = AssignmentState(init_assign, None, get_modified_locs(init_assign, predictions))

    logging.debug(init_state.to_string())

    #Modify the source code with the new type assignment and get the score
    source_rep.insert_assignment(init_state.assignment)
    init_state.score, error_locs = type_check(source_rep, w, w-sum((len(v) for v in init_state.assignment.values())))

    logging.debug(init_state.score)
    logging.debug(error_locs)

    #Get the child states from the current state and add them to the work set
    work_set = new_states(init_state, predictions)
    done = {init_state}

    #While a perfect assignment (score = 0) hasn't been found and there are still states to explore
    while min((x.score for x in done)) > 0 and len(work_set) != 0:
        logging.debug("-------------------")
        for mm in work_set:
            logging.debug(mm.to_string())
        logging.debug("-------------------")

        #Pick a new state from the work set
        new_state = pick(work_set, error_locs)
        work_set.remove(new_state)
        logging.debug(new_state.to_string())

        #Insert the picked assignent into the source code and get the score
        source_rep.insert_assignment(new_state.assignment)
        new_state.score, error_locs  = type_check(source_rep,w, w-sum(len(v) for v in new_state.assignment.values()))

        logging.debug(new_state.score)
        logging.debug(error_locs)
        done.add(new_state)
      
        #Update the work set based on the results and if the greedy method is being used
        if greedy and new_state.score < new_state.parent.score:
            work_set = new_states(new_state, predictions)
        else:
            new_set = new_states(new_state, predictions)
            new_set.difference_update(done)
            work_set.update(new_set)
    #Find the state with the lowest score in the done set and return the assignment
    min_state = min(done, key=lambda x: x.score)
    for f_name in min_state.assignment.keys():
        for var in min_state.assignment[f_name].keys():
            loc = predictions[f_name][var][1]
            min_state.assignment[f_name][var] = (min_state.assignment[f_name][var], loc)
    return min_state.assignment


def type_check(source_rep: SourceRepresentation, weight: int, missing: int) -> int:
    '''
    Returns the score for the current type assignment

    :source_rep: The containing class for the altered source code
    :weight: The weight given to the number of errors
    :missing: The number of missing type assignments

    :returns: A score for the type assignment, higher is worse
    '''
    num_errors, error_locs = source_rep.get_type_errors()

    return missing+(weight*num_errors), error_locs


def get_top_predictions(predictions: Dict[str, Dict[str, Tuple[Dict[str, int],int]]]) -> Dict[str, Dict[str,str]]:
    '''
    Get the top prediction for each assignment

    :predictions: List of predicted type assignments for the variables
    :returns: An assigment of each open type to the top ranked predictions
    '''
    assignment = {}
    for f_name, open_types in predictions.items():
        assignment[f_name] = {}
        for type_name, (type_dict, _) in open_types.items():
            top_prediction = min(type_dict.keys(), key=(lambda k: type_dict[k]))
            assignment[f_name][type_name] = top_prediction
    return assignment

def new_states(assignment: Dict[str, Dict[str, str]], predictions: Dict[str, Dict[str, Tuple[Dict[str, int],int]]]) -> Set[AssignmentState]:
    '''
    Creates a set of new states based on the given assignment

    :assignment: Current assignment of the parent state
    :predictions: List of ranked type predictions

    :returns: Returns a set of assignment states
    '''
    children = set()
    #For each open type assignment
    for f_name, func_predictions in predictions.items():
        for v_name, (type_list, loc)in func_predictions.items():
            #If that type assignment is in the parent assignment
            if f_name in assignment.assignment:
                if v_name in assignment.assignment[f_name]:
                    #Get the current prediction and rank
                    current_prediction = assignment.assignment[f_name][v_name]
                    rank = type_list[current_prediction]
                    #For each type assignment of greater rank for that open type create a child state with state assignment
                    for typ,rnk in type_list.items():
                        if rnk > rank:
                            new_assignment = copy.deepcopy(assignment.assignment)
                            new_assignment[f_name][v_name] = typ
                            child = AssignmentState(new_assignment, assignment, get_modified_locs(new_assignment, predictions))
                            children.add(child)
                    new_assignment = copy.deepcopy(assignment.assignment)
                    del new_assignment[f_name][v_name]
                    if len(new_assignment[f_name]) == 0:
                        del new_assignment[f_name]
                    #Create a child where the open type is not assigned to
                    child = AssignmentState(new_assignment, assignment, get_modified_locs(new_assignment, predictions))
                    children.add(child)
    return children

def get_modified_locs(assignment: Dict[str, Dict[str, str]], predictions: Dict[str, Dict[str, Tuple[Dict[str, int],int]]]) -> Set[int]:
    '''
    Gets the list of locations modified by the type assignment

    :assignment: The current assignment
    :predictions: List of open type predictions and their locations

    :returns: A list of locations which the assignment modifies
    '''
    m_locs = set()
    for f_name, f_pred in assignment.items():
        for v_name, _ in f_pred.items():
           loc = predictions[f_name][v_name][1][0]
           m_locs.add(loc)

    return m_locs

def pick(children: Set[AssignmentState], error_locs: List[int]) -> AssignmentState:
    '''
    Given a set of AssignmentStates, returns a biased random one based on their weights

    :children: A set of AssignmentStates to be picked from
    :error_locs: A list of error locations from the current type assignment

    :returns: A picked assignment state
    '''
    rel_weights = []
    for state in children:
        rel_weights.append(calculate_weight(state, error_locs))
    picked_state = random.choices(list(children), rel_weights)
    return picked_state[0]

def calculate_weight(state: AssignmentState, error_locs: List[int]) -> int:
    '''
    Gives the state a weight based on whether it adds new assignments over its parent state
    and whether it modifies an assignment close to an error location

    :state: AssignmentState to calculate the weight
    :error_locs: A list of error locations from the current type assignment

    :returns: weight for the current assignment
    '''
    weight = sum((len(v) for v in state.assignment.values()))*2
    for loc in error_locs:
        for m_loc in state.modified_locs:
            if m_loc in (loc-1, loc+1):
                weight = weight+1
                break;

    return weight
