import ast
import copy
import astor
import re
import os
import sys
from mypy import api
from src.source_rep import SourceRepresentation
import logging
logging.basicConfig(level='DEBUG')
from tempfile import NamedTemporaryFile

class TypeReplacer(ast.NodeTransformer):
    '''
    Replaces nodes in the python AST with a type assignment
    '''
    def __init__(self):
        self.assignment = None
        self.func_assignment = None
        super()

    def set_assignment(self, assignment):
        self.assignment = assignment

    def visit_arg(self, node):
        if node.arg in self.func_assignment:
            return ast.copy_location(ast.arg(node.arg, self.func_assignment[node.arg]), node)
        else:
           return node

    def visit_FunctionDef(self, node):
        if node.name in self.assignment:
            self.func_assignment = self.assignment[node.name]
            self.generic_visit(node)
            if 'return' in self.func_assignment:
                return ast.copy_location(ast.FunctionDef(name = node.name, args = node.args, body = node.body, decorator_list = node.decorator_list, returns = self.func_assignment['return'], type_comment = node.type_comment), node)
        return node

    def visit_AsyncFunctionDef(self, node):
        if node.name in self.assignment:
            self.func_assignment = self.assignment[node.name]
            self.generic_visit(node)
            if 'return' in self.func_assignment:
                return ast.copy_location(ast.AsyncFunctionDef(name = node.name, args = node.args, body = node.body, decorator_list = node.decorator_list, returns = self.func_assignment['return'], type_comment = node.type_comment), node)
        return node


class PythonSourceRepresentation(SourceRepresentation):
    '''
    A python type representation, initialized with a path to source file
    '''
    def __init__(self, source_file):
        with open(source_file, 'r') as source:
            self.tree = ast.parse(source.read(), mode='exec', type_comments=True)
        self.replacer = TypeReplacer()
        self.updated_ast = None
        
    def insert_assignment(self, assignment):
        '''
        Given an assignment, inserts the assignment into the source code representation (a python ast)
        '''
        self.replacer.set_assignment(assignment)
        clean_ast = copy.deepcopy(self.tree)
        self.updated_ast = self.replacer.visit(clean_ast)

    def get_type_errors(self):
        '''
        Uses mypy to analyze the source code and get the errors from the type assignment
        '''
        #Write the AST out to source code
        with NamedTemporaryFile(mode='w+t') as f:
            f.write(astor.to_source(self.updated_ast))
            f.flush()
            #When inserting type assignments, imports from the typing module need to be added or else
            #we will get errors, so analyze the type assignment to insert import statements
            f_name=f.name
            result = api.run([f_name])
            output = result[0]
            lines = output.splitlines()
            import_needed = set()

            #For each error returned, check to see if it's a 'typing' import error and insert that import to the 
            #top of the temporary source file
            oline = f.readlines()
            for line in lines:
                imp = re.search("from typing import ([a-zA-Z]*)",line)
                if imp and imp not in import_needed:
                    import_needed.add(imp)
                    imp_line = imp.group(0)
                    oline.insert(0, imp_line+"\n")


            import_offset = len(import_needed)


            #Run type analysis again and get the new set of errors
            f.writelines(oline)
            f.flush()

            result = api.run([f_name])

            logging.debug(result[0])

            #For each error, get the line number for an error location
            output = result[0]
            lines = output.splitlines()
            error_locs = []
            for line in lines:
                error_loc = re.search(":([0-9]+)", line)
                if error_loc:
                    error_locs.append(int(error_loc.group(1))-import_offset)

            num_errors = re.search("Found ([0-9]+)",result[0])

            if num_errors:
                found_errors = int(num_errors.group(1))
            else:
                found_errors = 0

            return found_errors, error_locs
        return  sys.maxsize,[]
