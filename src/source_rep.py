import abc

class SourceRepresentation(abc.ABC):
    '''
    Interface used by type_infer to represent and interact with the source code and do type checking
    A subclass of SourceRepresentation must include get_type_errors and insert_assignment methods
    '''
    def __init__(self, source_file):
        pass

    @abc.abstractmethod
    def insert_assignment(self, assignment):
        pass

    @abc.abstractmethod
    def get_type_errors(self):
        pass
